import tensorflow as tf
import numpy as np


###############

class hc_LSTMCell (tf.contrib.rnn.BasicLSTMCell):
    def __init__(self, num_units, forget_bias=1.0, state_is_tuple=True, activation=None, reuse=None):
        self._num_units = num_units
        super (hc_LSTMCell, self).__init__ (num_units, forget_bias,
                                            state_is_tuple, activation, reuse)

    #     @property
    #     def state_size(self):
    #         return 2 * self._num_units

    @property
    # Return h + c
    def output_size(self):
        return 2 * self._num_units

    def call(self, inputs, inp_state):
        with tf.variable_scope (type (self).__name__):
            op, cell_state = super (hc_LSTMCell, self).call (inputs, inp_state)
            h_c = tf.concat ([inp_state.h, inp_state.c], axis=1)
            return h_c, cell_state


###############

class model_4:
    def __init__(self, x_exog_1_count, x_exog_2_count, x_3_count, time_step, rnn_1_size, rnn_2_size,
                 rnn_3_size, num_layers_1, rnn_4_size, num_layers_4, learning_rate, dropout_prob, name='model'):
        self.name = name
        self.scope = name
        self.set_hyperparams (x_exog_1_count, x_exog_2_count, x_3_count, time_step, rnn_1_size, rnn_2_size,
                              rnn_3_size, num_layers_1, rnn_4_size, num_layers_4, learning_rate, dropout_prob)

        # tf.reset_default_graph()
        # self.graph = tf.Graph()


        with tf.variable_scope (self.scope):
            self.build_graph ()

        self.session = tf.Session ()
        # self.session = tf.Session (graph=self.graph)
        self.session.run (tf.global_variables_initializer ())
        return

    def close_session(self):
        writer = tf.summary.FileWriter ('./graphs', self.session.graph)
        writer.close ()
        self.session.close ()
        # use : tensorboard --logdir="./graphs" --port 6006
        return

    def set_hyperparams(self, x_exog_1_count, x_exog_2_count, x_3_count, time_step, rnn_1_size, rnn_2_size, rnn_3_size,
                        num_layers_1, rnn_4_size, num_layers_4, learning_rate, dropout_prob):

        self.x_exog_1_count = x_exog_1_count
        self.x_exog_2_count = x_exog_2_count
        self.x_3_count = x_3_count
        self.time_step = time_step

        self.rnn_1_size = rnn_1_size
        self.rnn_2_size = rnn_2_size
        self.rnn_3_size = rnn_3_size
        self.num_layers_1 = num_layers_1
        self.num_layers_4 = num_layers_4
        self.rnn_4_size = rnn_4_size
        self.dropout_prob = dropout_prob
        self.learning_rate = learning_rate
        return

    def lstm_layer(self, num_layers, rnn_size, state_is_tuple=True):
        cells = []

        for i in range (num_layers):
            cell = tf.contrib.rnn.BasicLSTMCell (
                rnn_size,
                forget_bias=1.0,
                state_is_tuple=state_is_tuple
            )
            cell = tf.nn.rnn_cell.DropoutWrapper (cell, self.dropout_prob)
            cells.append (cell)

        rnn_cell = tf.contrib.rnn.MultiRNNCell (cells, state_is_tuple=state_is_tuple)
        return rnn_cell

    def gru_layer(self, num_layers, rnn_size):
        cells = []

        for i in range (num_layers):
            cell = tf.contrib.rnn.GRUCell (num_units=rnn_size)
            cell = tf.nn.rnn_cell.DropoutWrapper (cell, self.dropout_prob)
            cells.append (cell)

        rnn_cell = tf.contrib.rnn.MultiRNNCell (cells)
        return rnn_cell

    def setup_init_states(self):
        self.rnn_1_last_state = None
        self.rnn_2_last_state = None
        self.rnn_3_last_state = None
        self.rnn_4_last_state = None

    def build_graph(self):
        # Weather
        # x_exog_1_count is number of of weather stats/features
        self.x_exog_1 = tf.placeholder (tf.float32, [None, self.time_step, self.x_exog_1_count])

        # GST
        # x_exog_2_count acts as lookback
        self.x_exog_2 = tf.placeholder (tf.float32, [None, self.time_step, self.x_exog_2_count])

        # wILI
        # x_3_count acts as lookback
        self.x_3 = tf.placeholder (tf.float32, [None, self.time_step, self.x_3_count])

        # True Value
        self.y = tf.placeholder (tf.float32, [None, self.time_step, self.x_3_count])

        # ------------- #
        # 3 parallel RNNs
        # ------------- #

        self.setup_init_states ()

        # ------ #
        # RNN 1

        self.rnn_1 = self.gru_layer (self.num_layers_1, self.rnn_1_size)
        rnn_1_inp = self.x_exog_1
        self.rnn_1_init_value = tf.placeholder (tf.float32, [None, self.num_layers_1, self.rnn_1_size])

        if self.rnn_1_last_state is None:
            init_state = None
        else:
            init_state = tuple (tf.unstack (self.rnn_1_init_value, axis=1))

        with tf.variable_scope ('rnn_1'):
            self.rnn_1_op, self.rnn_1_state = tf.nn.dynamic_rnn (
                cell=self.rnn_1,
                inputs=rnn_1_inp,
                initial_state=init_state,
                dtype=tf.float32
            )
        print 'rnn_1_op ', self.rnn_1_op.shape
        print 'rnn_1_state ', tf.stack (list (self.rnn_1_state), axis=1).shape

        # ------ #
        # RNN 2
        self.rnn_2 = self.gru_layer (self.num_layers_1, self.rnn_2_size)
        rnn_2_inp = self.x_exog_2
        self.rnn_2_init_value = tf.placeholder (tf.float32, [None, self.num_layers_1, self.rnn_2_size])

        if self.rnn_2_last_state is None:
            init_state = None
        else:
            init_state = tuple (tf.unstack (self.rnn_2_init_value, axis=1))

        with tf.variable_scope ('rnn_2'):
            self.rnn_2_op, self.rnn_2_state = tf.nn.dynamic_rnn (
                cell=self.rnn_2,
                inputs=rnn_2_inp,
                initial_state=init_state,
                dtype=tf.float32
            )

        print 'rnn_2_op', self.rnn_2_op.shape
        print 'rnn_2_state', tf.stack (list (self.rnn_2_state), axis=1).shape

        # ------ #
        # RNN 3
        self.rnn_3 = self.gru_layer (self.num_layers_1, self.rnn_3_size)
        rnn_3_inp = self.x_3
        self.rnn_3_init_value = tf.placeholder (tf.float32, [None, self.num_layers_1, self.rnn_3_size])
        if self.rnn_3_last_state is None:
            init_state = None
        else:
            init_state = tuple (tf.unstack (self.rnn_3_init_value, axis=1))

        with tf.variable_scope ('rnn_3'):
            self.rnn_3_op, self.rnn_3_state = tf.nn.dynamic_rnn (
                cell=self.rnn_3,
                inputs=rnn_3_inp,
                initial_state=init_state,
                dtype=tf.float32
            )

        print 'rnn_3_op ', self.rnn_3_op.shape

        cur_op = tf.concat ([self.rnn_1_op, self.rnn_2_op, self.rnn_3_op], axis=-1)
        dim_1 = cur_op.shape.as_list ()[-1]

        proj_dim = 32
        W_1 = tf.Variable (
            initial_value=tf.random_normal ([dim_1, proj_dim], stddev=0.1),
            name='W_1',
            dtype=tf.float32,
            trainable=True)
        B_1 = tf.Variable (
            initial_value=tf.random_normal ([proj_dim], stddev=0.1),
            name='B_1',
            dtype=tf.float32,
            trainable=True)

        cur_op = tf.unstack (cur_op, axis=1)
        proj_concat = [None] * self.time_step
        for ts in range (self.time_step):
            proj_concat[ts] = tf.nn.tanh (tf.nn.xw_plus_b (cur_op[ts], W_1, B_1))

        self.cur_op_1 = tf.stack (proj_concat, axis=1)
        print self.cur_op_1

        # Decoder
        self.rnn_4 = self.gru_layer (self.num_layers_4, self.rnn_4_size)
        rnn_4_inp = tf.concat ([self.cur_op_1], axis=-1)
        self.rnn_4_init_value = tf.placeholder (tf.float32, [None, self.num_layers_4, self.rnn_4_size])

        if self.rnn_1_last_state is None:
            init_state = None
        else:
            init_state = tuple (tf.unstack (self.rnn_4_init_value, axis=1))

        with tf.variable_scope ('rnn_4'):
            self.rnn_4_op, self.rnn_4_state = tf.nn.dynamic_rnn (
                cell=self.rnn_4,
                inputs=rnn_4_inp,
                initial_state=init_state,
                dtype=tf.float32
            )

        print 'rnn_4_op', self.rnn_4_op.shape

        W_2 = tf.Variable (
            initial_value=tf.random_normal ([self.rnn_4_size, self.x_3_count], stddev=0.1),
            name='W_2',
            dtype=tf.float32,
            trainable=True)

        B_2 = tf.Variable (
            initial_value=tf.random_normal ([self.x_3_count], stddev=0.1),
            name='B_2',
            dtype=tf.float32,
            trainable=True)

        self.cur_op = self.rnn_4_op
        self.next_op = [None] * self.time_step
        self.cur_op = tf.unstack (self.cur_op, axis=1)

        for ts in range (self.time_step):
            self.next_op[ts] = tf.nn.relu (tf.nn.xw_plus_b (self.cur_op[ts], W_2, B_2))

        self.pred = tf.stack (self.next_op, axis=1)
        print 'pred shape', self.pred.shape

        self.loss = tf.losses.mean_squared_error (self.pred, self.y)

        optimizer = tf.train.AdamOptimizer (self.learning_rate)
        gradients, variables = zip (*optimizer.compute_gradients (self.loss))
        gradients, _ = tf.clip_by_global_norm (gradients, 5.0)
        self.optimize = optimizer.apply_gradients (zip (gradients, variables))
        return


    def train_batch(self, data_x1, data_x2, data_x3, data_y):

        # This is for the first run when no previous state exists
        if self.rnn_1_last_state is None:
            batch_size = self.session.run (tf.shape (data_x1))[0]
            rnn_1_last_state = np.zeros ([batch_size, self.num_layers_1, self.rnn_1_size])
            rnn_2_last_state = np.zeros ([batch_size, self.num_layers_1, self.rnn_2_size])
            rnn_3_last_state = np.zeros ([batch_size, self.num_layers_1, self.rnn_3_size])
            rnn_4_last_state = np.zeros ([batch_size, self.num_layers_1, self.rnn_4_size])
        else:
            rnn_1_last_state = self.rnn_1_last_state
            rnn_2_last_state = self.rnn_2_last_state
            rnn_3_last_state = self.rnn_3_last_state
            rnn_4_last_state = self.rnn_4_last_state

        # ------------------ #
        rnn_1_state, rnn_2_state, rnn_3_state, rnn_4_state, _, loss ,pred = self.session.run (
            [self.rnn_1_state, self.rnn_2_state, self.rnn_3_state, self.rnn_4_state, self.optimize, self.loss ,self. pred],
            feed_dict={self.x_exog_1: data_x1,
                       self.x_exog_2: data_x2,
                       self.x_3: data_x3,
                       self.y: data_y,
                       self.rnn_1_init_value: rnn_1_last_state,
                       self.rnn_2_init_value: rnn_2_last_state,
                       self.rnn_3_init_value: rnn_3_last_state,
                       self.rnn_4_init_value: rnn_4_last_state,
                       })

        # Reshape the stae outputs so as to store them as a tensor and feed them in the next iteration
        self.rnn_1_last_state = np.stack (list (rnn_1_state), axis=1)
        self.rnn_2_last_state = np.stack (list (rnn_2_state), axis=1)
        self.rnn_3_last_state = np.stack (list (rnn_3_state), axis=1)
        self.rnn_4_last_state = np.stack (list (rnn_4_state), axis=1)


        return loss, pred


##############################



def dummy_test():
    learning_rate = 0.05
    dropout_prob = 0.075
    x_exog_1_count = 10
    x_exog_2_count = 5
    x_3_count = 5
    time_step = 7
    rnn_1_size = 32
    rnn_2_size = 32
    rnn_3_size = 32
    num_layers_1 = 2
    rnn_4_size = 64
    num_layers_4 = 2
    m1 = model_4 (x_exog_1_count, x_exog_2_count, x_3_count, time_step, rnn_1_size, rnn_2_size,
                  rnn_3_size, num_layers_1, rnn_4_size, num_layers_4, learning_rate, dropout_prob
                  )
    data_x1 = np.random.rand (100, time_step, x_exog_1_count)
    data_x2 = np.random.rand (100, time_step, x_exog_2_count)
    data_x3 = np.random.rand (100, time_step, x_3_count)
    data_y = np.random.rand (100, time_step, 1)
    m1.train_batch (data_x1, data_x2, data_x3, data_y)
    m1.train_batch (data_x1, data_x2, data_x3, data_y)
    m1.train_batch (data_x1, data_x2, data_x3, data_y)
    m1.close_session ()

# dummy_test()
