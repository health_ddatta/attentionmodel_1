import tensorflow as tf
import numpy as np


###############

class hc_LSTMCell(tf.contrib.rnn.BasicLSTMCell):
    def __init__(self, num_units, forget_bias=1.0, state_is_tuple=True, activation=None, reuse=None):
        self._num_units = num_units
        super(hc_LSTMCell, self).__init__(num_units, forget_bias,
                                          state_is_tuple, activation, reuse)

    #     @property
    #     def state_size(self):
    #         return 2 * self._num_units

    @property
    # Return h + c
    def output_size(self):
        return 2 * self._num_units

    def call(self, inputs, inp_state):
        with tf.variable_scope(type(self).__name__):
            op, cell_state = super(hc_LSTMCell, self).call(inputs, inp_state)
            h_c = tf.concat([inp_state.h, inp_state.c], axis=1)
            return h_c, cell_state


###############

class model:
    def __init__(self, num_series, num_ts, lookback, rnn_1_size, rnn_2_layers, rnn_2_size, rnn_3_layers,rnn_3_size, learning_rate, dropout_prob, name='model'):
        self.name = name
        self.scope = name
        self.set_hyperparams(num_series, num_ts, lookback, rnn_1_size, rnn_2_layers, rnn_2_size,rnn_3_layers,rnn_3_size, learning_rate, dropout_prob)

        # tf.reset_default_graph()
        # self.graph = tf.Graph()
        # self.session = tf.Session(graph=self.graph)

        with tf.variable_scope(self.scope):
            self.build_graph()

        self.session = tf.Session()
        self.session.run(tf.global_variables_initializer())
        return

    def close_session(self):
        writer = tf.summary.FileWriter('./graphs', self.session.graph)
        writer.close()
        self.session.close()
        # use : tensorboard --logdir="./graphs" --port 6006
        return

    def set_hyperparams(self, num_series, num_ts, lookback, rnn_1_size, rnn_2_layers, rnn_2_size,rnn_3_layers,rnn_3_size , learning_rate, dropout_prob):
        self.num_series = num_series
        self.num_ts = num_ts
        self.lookback = lookback
        self.rnn_1_size = rnn_1_size
        self.dropout_prob = dropout_prob
        self.rnn_2_layers = rnn_2_layers
        self.rnn_2_size = rnn_2_size
        self.rnn_3_layers = rnn_3_layers
        self.rnn_3_size = rnn_3_size
        self.learning_rate = learning_rate
        return

    def lstm_layer(self, num_layers, rnn_size, state_is_tuple=True):
        cells = []

        for i in range(num_layers):
            cell = tf.contrib.rnn.BasicLSTMCell(
                rnn_size,
                forget_bias=1.0,
                state_is_tuple=state_is_tuple
            )
            cell = tf.nn.rnn_cell.DropoutWrapper(cell, self.dropout_prob)
            cells.append(cell)

        rnn_cell = tf.contrib.rnn.MultiRNNCell(cells, state_is_tuple=state_is_tuple)
        return rnn_cell

    def gru_layer(self, num_layers, rnn_size):
        cells = []

        for i in range(num_layers):
            cell = tf.contrib.rnn.GRUCell(num_units=rnn_size)
            cell = tf.nn.rnn_cell.DropoutWrapper(cell, self.dropout_prob)
            cells.append(cell)

        rnn_cell = tf.contrib.rnn.MultiRNNCell(cells)
        return rnn_cell

    def build_graph(self):

        # num_ts is the window till which to backpropagate error
        # Attention to be calculated at each time step (num_ts)
        self.x_exog = tf.placeholder(tf.float32, [None, self.num_ts, self.num_series, self.lookback])
        print  'Exog inp : ' , self.x_exog.shape
        self.x_main = tf.placeholder(tf.float32, [None, self.num_ts, 1])
        print  'Main inp : ', self.x_main
        self.y = tf.placeholder(tf.float32, [None, self.num_ts, 1])
        x_exog_last_x = self.x_exog[:, :, :, -1]
        rnn_1_inp = x_exog_last_x

        print 'rnn_1_inp ', rnn_1_inp.shape
        self.rnn_1_init_value = None
        self.rnn_1 = hc_LSTMCell (num_units=self.rnn_1_size)

        with tf.variable_scope('rnn_1'):
           rnn_1_op, rnn_1_state = tf.nn.dynamic_rnn(
                self.rnn_1,
                rnn_1_inp,
                self.rnn_1_init_value,
                dtype=tf.float32
            )

        '''
        For attention layer weights  are :
        V_e
        W_e [h(t-1) s(t-1)]
        U_e [x]
        '''

        U_e = [tf.Variable(
            initial_value=tf.random_normal([self.lookback, self.lookback], stddev=0.1),
            name='U_e',
            dtype=tf.float32,
            trainable=True
        )] * self.num_series
        V_e = tf.Variable(
            initial_value=tf.random_normal([self.lookback, 1], stddev=0.1),
            name='W_e',
            dtype=tf.float32,
            trainable=True)
        W_e = tf.Variable(
            initial_value=tf.random_normal([self.rnn_1_size * 2, self.lookback], stddev=0.1),
            name='W_e',
            dtype=tf.float32,
            trainable=True)
        t1 = [None] * self.num_ts
        t2 = [None] * self.num_ts

        print 'U_e', len(U_e) , U_e[0].shape
        for time_step in range(self.num_ts):
            rnn_1_ts_op = rnn_1_op[:, time_step, :]
            t1[time_step] = tf.nn.xw_plus_b(rnn_1_ts_op, W_e, tf.zeros(self.lookback))

        for time_step in range(self.num_ts):
            x_exog_ts = self.x_exog[:, time_step, :]
            # print 'x_exog_ts', x_exog_ts.get_shape()
            all_series_res = [None] * self.num_series
            for series in range(self.num_series):
                x_exog_ts_series = x_exog_ts[:, series, :]
                # print 'x_exog_ts_series', x_exog_ts_series.get_shape()
                res = tf.nn.xw_plus_b(x_exog_ts_series, U_e[series], tf.zeros(self.lookback))
                # print 'res' , res.get_shape()

                # W_e [...] + U_e(series)[...]
                res = tf.add(res, t1[time_step])
                all_series_res[series] = res
            t2[time_step] = all_series_res

        self.t3 = [None] * self.num_ts
        for time_step in range(self.num_ts):
            self.t3[time_step] = [None] * self.num_series
            for series in range(self.num_series):
                res = tf.tanh(t2[time_step][series])
                # tanh (W_e [...] + U_e(series)[...]) * V_e
                res = tf.matmul(res, V_e)
                self.t3[time_step][series] = res
            self.t3[time_step] = tf.nn.softmax(self.t3[time_step])
            self.t3[time_step] = tf.transpose(self.t3[time_step], [1, 0, 2])

        self.att_wts = tf.stack(self.t3, axis=0)
        self.att_wts = tf.transpose(self.att_wts, [1, 0, 2, 3])
        self.att_wts = tf.squeeze(self.att_wts, axis=-1)
        print  self.att_wts

        # Multiply the attention weights
        self.encoder_inp = tf.multiply(x_exog_last_x, self.att_wts)

        print self.encoder_inp
        # build the encoder
        self.rnn_2 = self.gru_layer(self.rnn_2_layers, self.rnn_2_size)
        self.rnn_2_init_value = None

        with tf.variable_scope('rnn_2'):
            self.rnn_2_output, self.rnn_2_state = tf.nn.dynamic_rnn(self.rnn_2,
                                                                    self.encoder_inp,
                                                                    initial_state=self.rnn_2_init_value,
                                                                    dtype=tf.float32)
        print ' rnn_2_output ', self.rnn_2_output.shape

        # concat the the rnn_2 op with x_main
        self.rnn_3_inp = tf.concat([self.rnn_2_output,self.x_main],axis = -1)
        print ' rnn_3_inp ' , self.rnn_3_inp.shape

        self.rnn_3= self.gru_layer (self.rnn_3_layers, self.rnn_3_size)
        with tf.variable_scope ('rnn_3'):
            self.rnn_3_output, self.rnn_3_state = tf.nn.dynamic_rnn (self.rnn_3,
                                                                     self.encoder_inp,
                                                                     initial_state=self.rnn_2_init_value,
                                                                     dtype=tf.float32)

        print 'rnn_3_output', self.rnn_3_output.shape

        W_y = tf.Variable(
            initial_value=tf.random_normal([self.rnn_3_size , 1], stddev=0.1),
            name='W_y',
            dtype=tf.float32,
            trainable=True)
        B_y = tf.Variable(
            initial_value=tf.random_normal([1], stddev=0.1),
            name='B_y',
            dtype=tf.float32,
            trainable=True)
        unstacked_rnn_3_output = tf.unstack(self.rnn_3_output,axis = 1)
        pred = [None] * self.num_ts

        for time_step in range(self.num_ts):
            pred[time_step] = tf.nn.xw_plus_b (unstacked_rnn_3_output[time_step], W_y, B_y)

        self.pred = tf.stack(pred,axis =1)
        print self.pred.shape
        self.loss = tf.losses.mean_squared_error(self.pred,self.y)

        optimizer = tf.train.AdamOptimizer (self.learning_rate)
        gradients, variables = zip (*optimizer.compute_gradients (self.loss))
        gradients, _ = tf.clip_by_global_norm (gradients, 5.0)
        self.optimize = optimizer.apply_gradients (zip (gradients, variables))
        return

    def run(self, x):
        self.session.run([self.t3],
                         {self.x_exog: x}
                         )


##############################

num_series = 10
num_ts = 8
lookback = 10
rnn_1_size = 64
rnn_2_layers = 2
rnn_2_size = 32
rnn_3_layers = 3
rnn_3_size = 64
learning_rate = 0.05
dropout_prob = 0.15

m1 = model(num_series, num_ts, lookback, rnn_1_size, rnn_2_layers, rnn_2_size,rnn_3_layers ,rnn_3_size, learning_rate, dropout_prob)
data = np.random.rand(100, num_ts, num_series, lookback)
m1.run(data)
m1.close_session()
