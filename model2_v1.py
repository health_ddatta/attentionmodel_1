import tensorflow as tf
import numpy as np
import tensorlayer as tl


class model:
    def __init__(self, num_series, num_ts, x_dim, rnn_1_num_layers, rnn_1_size, dropout_prob, name='model'):
        self.name = name
        self.scope = name
        tf.reset_default_graph()
        self.graph = tf.Graph()
        self.session = tf.Session(graph=self.graph)
        self.set_hyperparams(num_series, num_ts, x_dim, rnn_1_num_layers, rnn_1_size, dropout_prob)
        with tf.variable_scope(self.scope):
            self.build_graph()
        return

    def close_session(self):
        self.session.close()

    def set_hyperparams(self, num_series, num_ts, x_dim, rnn_1_num_layers, rnn_1_size, dropout_prob):
        self.num_series = num_series
        self.num_ts = num_ts
        self.x_dim = x_dim
        self.rnn_1_num_layers = rnn_1_num_layers
        self.rnn_1_size = rnn_1_size
        self.dropout_prob = dropout_prob
        return

    def rnn_layer(self, num_layers, rnn_size, type='LSTM'):
        cells = []
        for i in range(num_layers):
            cell = None
            if type == 'LSTM':
                cell = tf.contrib.rnn.BasicLSTMCell(
                    rnn_size,
                    forget_bias=0.0,
                    state_is_tuple=True
                )
                state_is_tuple = True

            elif type == 'GRU':
                cell = tf.contrib.rnn.GRUCell(num_units=rnn_size)
                state_is_tuple = False

            cell = tf.nn.rnn_cell.DropoutWrapper(cell, self.dropout_prob)
            cells.append(cell)

        rnn_cell = tf.contrib.rnn.MultiRNNCell(cells, state_is_tuple=state_is_tuple)
        return rnn_cell

    def build_graph(self):

        # Input should be N time series (exogenous (N-1) + target (1) )
        # size [N]
        # [batch , # of time-step , input]

        self.x = tf.placeholder(tf.float32,
                                [None,
                                 self.num_series,
                                 self.num_ts,
                                 self.x_dim
                                 ],
                                name="x")

        # Create a rnn for each series
        self.rnn_1 = [None] * self.num_series
        for i in range(self.num_series):
            var_scope = 'rnn_1_'+str(i)
            with tf.variable_scope(var_scope):
                _rnn_1_i = self.rnn_layer(self.rnn_1_num_layers, self.rnn_1_size, type='GRU')
                self.rnn_1[i] = _rnn_1_i


        #
        # rnn1_init_state = tf.placeholder(tf.float32, [self.rnn_1_num_layers, None, self.rnn_1_size])
        # state_per_layer_list = tf.unstack(rnn1_init_state, axis=0)
        #
        # self.rnn_1_init_value = tuple(
        #     [tf.nn.rnn_cell.LSTMStateTuple(state_per_layer_list[idx][0], state_per_layer_list[idx][1])
        #      for idx in range(self.rnn_1_num_layers)]
        # )

        self.rnn_1_init_value = [None] * self.num_series
        for i in range(self.num_series):
            _rnn_1_init_value = tf.placeholder(tf.float32, [None, self.rnn_1_num_layers * self.rnn_1_size])
            self.rnn_1_init_value[i] =_rnn_1_init_value

        rnn_1_op = [None] * self.num_series
        rnn_1_state = [None] * self.num_series

        for i in range(self.num_series):
            var_scope = 'rnn_1_' + str(i)
            with tf.variable_scope(var_scope):
                _x_inp = self.x[:, i, :, :]
                # _x_inp = tf.squeeze(_x_inp, axis=2)
                _rnn_1_op, _rnn_1_state = tf.nn.dynamic_rnn(
                    self.rnn_1[i],
                    _x_inp,
                    initial_state = self.rnn_1_init_value[i],
                    dtype=tf.float32
                )
                rnn_1_op[i] = _rnn_1_op
                rnn_1_state[i] = _rnn_1_state

        print 'Shape of rnn_1_op', rnn_1_op[0].shape
        print 'Shape of rnn_1_state ', rnn_1_state

        # Attention weights :
        # v_e, W_e, U_e
        V_e = [None] * self.num_series
        W_e = [None] * self.num_series
        U_e = [None] * self.num_series

        for i in range(self.num_series):
            var_scope = 'rnn_1_' + str(i)
            with tf.variable_scope(var_scope):
                V_e[i] = tf.Variable(
                    initial_value=tf.random_normal([1,1], stddev=0.1),
                    name='V_e',
                    dtype=tf.float32,
                )
                W_e[i] = tf.Variable(
                    initial_value=tf.random_normal([rnn_1_size,1], stddev=0.1),
                    name='W_e',
                    dtype=tf.float32,
                )
                U_e[i] = tf.Variable(
                    initial_value=tf.random_normal([self.x_dim,1], stddev=0.1),
                    name='U_e',
                    dtype=tf.float32,
                )


        # e^k = V_e tanh( W_e *h_(t-1) + U_e* x^k)
        # h(t-1) is [ batch, ts , rnn_1_size]
        # W_e is rnn_1_size
        # x^k is [x_dim]
        # for each time step there is a h(t-1)

        res = [None] * self.num_series
        # i th series
        for i in range(self.num_series):
            var_scope = 'rnn_1_' + str(i)
            _x_i = self.x[:, i, :, :]

            with tf.variable_scope(var_scope):
                # Calculate the attention weights
                # i th GRU
                _rnn_1_i_op = rnn_1_op[i]

                res[i] = [None] * self.num_ts
                for j in range(self.num_ts):
                    h_prev = _rnn_1_i_op[:,j,:]
                    _x_i_j = _x_i[:,j,:]
                    res_1 = tf.nn.xw_plus_b ( h_prev,  W_e[i] , tf.zeros(1) )
                    res_2 = tf.nn.xw_plus_b ( _x_i_j , U_e[i] , tf.zeros(1) )
                    res_3 = V_e[i] * tf.tanh(res_1 + res_2)
                    res[i][j] = res_3

        # res has the attention weights for ith series at jth timestep
        # do a softmax
        # get the attention weights

        return

    def save_model(self):
        saver = tf.train.Saver()
        saver.save(self.session, self.name)

    def train_model(self):
        return

    def run_model(self):
        return


def gen_data():
    x = np.random.normal([10, 3, 8])
    return x


num_series = 8
num_ts = 5
x_dim = 3
rnn_1_num_layers = 3
rnn_1_size = 128
dropout_prob = 0.01

model_obj = model(num_series, num_ts, x_dim, rnn_1_num_layers, rnn_1_size, dropout_prob)
