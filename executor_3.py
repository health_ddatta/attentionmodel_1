import model_4_v1
import config
import data_feeder
import matplotlib.pyplot as plt
import os

f_s_map = config.feature_stat_map
region = config.region



def plot_xy(x,y1,y2):
    plt.plot (x, y1, 'ro-')
    plt.plot (x, y2, 'bo-')
    plt.xlabel ('Week')
    plt.show()


def get_model_params():
    learning_rate = config.learning_rate
    dropout_prob = config.dropout_prob
    x_exog_1_count = config.weather_lookback
    x_exog_2_count = config.gst_lookback
    x_3_count = config.ili_lookback
    time_step = config.time_step
    rnn_1_size = config.rnn_1_size
    rnn_2_size = config.rnn_2_size
    rnn_3_size = config.rnn_3_size
    num_layers_1 = config.num_layers_1
    rnn_4_size = config.rnn_4_size
    num_layers_4 = config.num_layers_4
    return x_exog_1_count, x_exog_2_count, x_3_count, time_step, rnn_1_size, rnn_2_size, rnn_3_size, num_layers_1, rnn_4_size, num_layers_4, learning_rate, dropout_prob

# --------------- #

x_exog_1_count, x_exog_2_count, x_3_count, time_step, rnn_1_size, rnn_2_size, rnn_3_size, \
num_layers_1, rnn_4_size, num_layers_4, learning_rate, dropout_prob = get_model_params ()

print x_3_count
model_obj = model_4_v1.model_4 (x_exog_1_count, x_exog_2_count, x_3_count, time_step, rnn_1_size, rnn_2_size,
                                rnn_3_size, num_layers_1, rnn_4_size, num_layers_4, learning_rate, dropout_prob)
df_obj = data_feeder.data_feeder_obj ()
num_batches = df_obj.num_batches


for epoch_num in range (config.max_epochs):
    track = False
    x = []
    y = []
    loss = 0

    if epoch_num % 1000 == 0:
        track = True

    for _ in range(num_batches):
        ili_x1, weather_x2, gst_x3, trian_y = df_obj.get_batch ()

        for _x1, _x2, _x3, _y in zip (ili_x1, weather_x2, gst_x3, trian_y):
            _loss,pred = model_obj.train_batch ( _x2, _x3, _x1, _y )

            if track:
                loss += _loss

                _tmp1 = []
                for _tmp in _x1[0]:
                  _tmp1.append(_tmp[-1])
                x.extend (_tmp1)

                _tmp1 = []
                for _tmp in _y[0]:
                  _tmp1.append(_tmp[-1])
                y.extend( _tmp1)

    if track :
        print '-----------'
        print 'Loss', loss/num_batches
        print 'Real (t)', len(x)
        print 'Pred (t+1)', len(y)
        print '-----------'
        y1 = x[1:]
        y2 = y[:-1]

        x = range(len(y1))
        #plot_xy(x,y1,y2)

