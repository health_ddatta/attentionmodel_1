stats= [
    'week_median',
    'week_max',
    'week_min' ,
    'week_variance',
    'week_max_daily_variation',
    'week_min_daily_mean' ]
features = ['abs_hum','rel_hum','sp_hum','temp','v_wind','u_wind']


feature_stat_map = {
    'abs_hum': ['week_median' ,'week_max_daily_variation'] ,
    'rel_hum': [],
    'sp_hum' : [],
    'temp': ['week_median'],
    'u_wind':[],
    'v_wind' : []
    }

max_epochs = 15000
region = 'nat'
start_year = 2003
end_year = 2015

# ------------------ #



#set dimension of weather data
def set_weather_dim():
    num_f = 0
    for k,v in feature_stat_map.iteritems():
        num_f +=len(v)
    return num_f


learning_rate = 0.075
dropout_prob = 0.05
weather_lookback =  set_weather_dim ()

ili_lookback = 8
gst_lookback = 4
ew_lookback = 4

time_step = 5
rnn_1_size = 32
rnn_2_size = 32
rnn_3_size = 32
num_layers_1 = 2
rnn_4_size = 64
num_layers_4 = 2


# ----- #

# num_f = 0
# for k,v in features_stat_map.iteritems():
#     num_f +=len(v)
#
# cnn_inp_height = 3
# cnn_inp_width = 8
# cnn_input_channels = num_f + 1
# start_year = 2003
# end_year = 2015
#
# batch_size = 8
#
# test_year = 2016