import sys
sys.path.append("./..")
sys.path.append("./../.")


import config
import model_1_v2 as model
import tensorflow as tf
import numpy as np
import data_feeder
import time
import seaborn as sns
import matplotlib.pyplot as plt
import os
import multiprocessing
from multiprocessing import Process
import exec_2_config


op_location = "./output"
def write_op(data_dict, LR):

    cwd = os.getcwd()
    global op_location
    os.chdir(op_location)

    directory = str(time.time())
    if not os.path.exists(directory):
        os.makedirs(directory)
    os.chdir(directory)

    file_name = str(time.time()) + '.txt'
    file_path = file_name
    f = open(file_path, 'w')

    eps = [ str(ep) for ep in data_dict.keys()]
    eps = ','.join(eps)
    f.write(eps)
    f.write("\n")

    vals = [str(val) for val in data_dict.values()]
    vals = ','.join(vals)
    f.write(vals)
    f.write("\n")

    f.write(str(config.features_stat_map))
    f.write("\n")

    f.close()
    file_name = str(time.time()) + '.png'
    plot(data_dict.keys(), data_dict.values(), file_name)


    os.chdir(cwd)
    return


def plot(x,y,file_name):
    sns.pointplot(x,y)
    plt.savefig(file_name)
    return


def process(f_s_map):

    num_channels = 0
    for k,v in f_s_map.iteritems():
        num_channels += len(v)
    num_channels += 1

    rows = config.cnn_inp_height
    lookback = config.cnn_inp_width
    learning_rate = 0.035

    max_epochs = 60
    disp_interval = 2

    session = tf.Session()
    network = model.Model(session, learning_rate, rows, lookback, num_channels, name="model_1")
    session.run(tf.initialize_all_variables())
    region = 'hhs3'

    loss_epoch_map = {}
    for epoch_num in range(max_epochs):

        epoch_x, epoch_y = data_feeder.get_one_epoch_data(region, f_s_map )
        for _x, _y in zip(epoch_x, epoch_y):
            loss = network.train_batch(_x, _y)

        if epoch_num % disp_interval == 0:
            print 'Loss', loss
            loss_epoch_map[epoch_num] = loss

    print os.getcwd()
    write_op(loss_epoch_map, learning_rate)


pool = []

for conf in exec_2_config.conf :
    p = Process( target = process, args = (conf, ) )
    pool.append(p)

for p in pool:
    p.start()

for p in pool:
    p.join()
