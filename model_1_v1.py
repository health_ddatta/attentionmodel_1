import tensorflow as tf
import numpy as np
import random


class Model:
    def max_ppol(self, x_, window_shape, dilation_rate=1, stride=[1, 1]):
        op = tf.nn.pool (
            input=x_,
            window_shape=window_shape,
            pooling_type='MAX',
            padding='VALID',
            dilation_rate=dilation_rate,
            strides=stride,
            name=None,
            data_format=None
        )
        return op

    def avg_ppol(self, x_, window_shape, dilation_rate=1, stride=[1, 1]):
        op = tf.nn.pool (
            input=x_,
            window_shape=window_shape,
            pooling_type='AVG',
            padding='VALID',
            dilation_rate=dilation_rate,
            strides=stride,
            name=None,
            data_format=None
        )
        return op

    # conv layer
    # Input : x_
    def conv2d(self, x_, filter_dim, img_dim, stride=1):

        filter_ht = filter_dim[0]
        filter_wd = filter_dim[1]
        filter_count = filter_dim[2]
        img_ht = img_dim[0]
        img_wd = img_dim[1]
        num_channels = img_dim[2]
        x_ = tf.reshape (x_, [-1, img_ht, img_wd, num_channels])

        activation = tf.layers.conv2d (
            inputs=x_,
            filters=filter_count,
            kernel_size=[filter_ht, filter_wd],
            strides=(stride, stride),
            padding='valid',
            data_format='channels_last',
            dilation_rate=(1, 1),
            activation=None,
            use_bias=True,
            kernel_initializer=None,
            bias_initializer=tf.zeros_initializer (),
            kernel_regularizer=None,
            bias_regularizer=None,
            activity_regularizer=None,
            kernel_constraint=None,
            bias_constraint=None,
            trainable=True,
            name=None,
            reuse=None
        )
        return activation

    def rnn_layer(self, num_layers, rnn_size, batch_size, type='LSTM'):
        cells = []
        init_state = []
        for i in range (num_layers):
            cell = None

            if type == 'LSTM':
                cell = tf.contrib.rnn.BasicLSTMCell (rnn_size, forget_bias=1.0, state_is_tuple=False)

            if type == 'GRU':
                cell = tf.contrib.rnn.GRUCell (rnn_size)

            init_state.append (cell.zero_state (batch_size, dtype=tf.float32))
            cells.append (cell)

        rnn_cell = tf.contrib.rnn.MultiRNNCell (cells, state_is_tuple=False)
        return rnn_cell

    def set_hyperparameters(self, name,session):
        # ---  hyperparameters --- #
        self.scope = name
        self.session = session
        self.learning_rate = 0.003

    def set_CNN_params(self):
        # CNN details
        self.conv_num_channels = 4
        self.conv_inp_rows = 3
        self.conv_lookback = 8
        self.conv_inpx_dim = [self.conv_inp_rows, self.conv_lookback, self.conv_num_channels]
        self.conv1_num_filter = 64

    def __init__(self, session, name="ModelInstance"):
        print 'Initialzing netwwork'

        self.set_hyperparameters (name,session)
        self.set_CNN_params ()

        # Encoder RNN details
        self.rnn1_count_hidden_types = 2
        self.rnn1_timestep = 4
        self.conv_inp_batch = self.rnn1_timestep * 5
        self.rnn1_batch_size = self.conv_inp_batch / self.rnn1_timestep

        # 1st RNN layer should have internally how many layers
        self.rnn1_num_layers = 3
        # 1st RNN layer should have internally how many states
        self.rnn1_size = 64
        self.rnn1_hidden_state_size = self.rnn1_num_layers * self.rnn1_count_hidden_types * self.rnn1_size

        #       Last state of rnn1, used when running the network in TEST mode
        self.rnn1_1_last_state = np.zeros ([self.rnn1_hidden_state_size, ])
        self.rnn1_2_last_state = np.zeros ([self.rnn1_hidden_state_size, ])

        # Decoder RNN details
        self.rnn2_count_hidden_types = 1
        self.rnn2_num_layers = 1
        self.rnn2_timestep = 1
        self.rnn2_size = 128
        self.rnn2_hidden_state_size = self.rnn2_num_layers * self.rnn2_count_hidden_types * self.rnn2_size
        self.rnn2_batch_size = self.rnn1_batch_size
        # Last state of rnn1, used when running the network in TEST mode
        self.rnn2_last_state = np.zeros ([self.rnn2_hidden_state_size, ])

        with tf.variable_scope (self.scope):
            # x : [ batch_size, timesteps, in_size ]
            # x is the input to conv layer
            self.x = tf.placeholder (tf.float32, [None, self.conv_inp_rows,
                                                  self.conv_lookback,
                                                  self.conv_num_channels],
                                     name="x")
            # y : [ batch_size, wILI Value ]
            # y is the next week's ILI value
            self.y = tf.placeholder (tf.float32, [None, 1])

            ########################



            filter_dim = [self.conv_inp_rows, 2, self.conv1_num_filter]
            self.conv1_op = self.conv2d (self.x,
                                         filter_dim,
                                         self.conv_inpx_dim,
                                         stride=1
                                         )
            window_shape = [1, 2]
            dilation_rate = [1, 1]
            self.maxp1_op = self.max_ppol (self.conv1_op, window_shape, dilation_rate)
            dilation_rate = [1, 1]
            self.avgp1_op = self.avg_ppol (self.conv1_op, window_shape, dilation_rate)

            # --- End of Convolution -- #

            # Use for rnn1_1 & rnn1_2
            self.rnn1_1_init_value = tf.placeholder (tf.float32,
                                                     shape=[None, self.rnn1_hidden_state_size],
                                                     name="rnn1_init_value")

            self.rnn1_2_init_value = tf.placeholder (tf.float32,
                                                     shape=[None, self.rnn1_hidden_state_size],
                                                     name="rnn1_init_value")

            # create 2 RNNs
            # _1 to accept max pool
            self.rnn1_1 = self.rnn_layer (num_layers=self.rnn1_num_layers,
                                          rnn_size=self.rnn1_size,
                                          batch_size=self.rnn1_batch_size
                                          )

            # _2 to accept avg pool
            self.rnn1_2 = self.rnn_layer (num_layers=self.rnn1_num_layers,
                                          rnn_size=self.rnn1_size,
                                          batch_size=self.rnn1_batch_size
                                          )

            # avg pool output is of the form :
            # [ batch size , height, width , channels]
            # convert it to [ batch , channels , height , width ]
            avgpool_op = tf.transpose (self.avgp1_op, [0, 3, 1, 2])
            rnn1_1_inp = avgpool_op
            maxpool_op = tf.transpose (self.maxp1_op, [0, 3, 1, 2])
            rnn1_2_inp = maxpool_op

            # ------------ Start : Encoder --------------#

            # features : flattened output from pool layer
            # reshape the output into shape [ rnn_batch_size, time_step, num_features ]
            num_features = rnn1_1_inp.get_shape ()[1].value * \
                           rnn1_1_inp.get_shape ()[2].value * \
                           rnn1_1_inp.get_shape ()[3].value

            rnn1_num_features = num_features / self.rnn1_timestep
            rnn1_1_inp = tf.reshape (rnn1_1_inp, [-1, num_features])
            rnn1_1_inp = tf.reshape (rnn1_1_inp, [-1, self.rnn1_timestep, rnn1_num_features])

            rnn1_2_inp = tf.reshape (rnn1_2_inp, [-1, num_features])
            rnn1_2_inp = tf.reshape (rnn1_2_inp, [-1, self.rnn1_timestep, rnn1_num_features])


            print 'rnn1_2_init_value shape',self.rnn1_2_init_value.get_shape().as_list()
            # Iteratively compute output of recurrent network
            with tf.variable_scope ('rnn1_1'):
                self.rnn1_1_output, self.rnn1_1_state = tf.nn.dynamic_rnn (self.rnn1_1,
                                                                           rnn1_1_inp,
                                                                           initial_state=self.rnn1_1_init_value,
                                                                           dtype=tf.float32)

            with tf.variable_scope ('rnn1_2'):
                self.rnn1_2_output, self.rnn1_2_state = tf.nn.dynamic_rnn (self.rnn1_2,
                                                                           rnn1_2_inp,
                                                                           initial_state=self.rnn1_2_init_value,
                                                                           dtype=tf.float32)

            self.rnn1_1_hidden_states = tf.reshape (self.rnn1_1_state,
                                                    shape=[-1, self.rnn1_num_layers * self.rnn1_count_hidden_types,
                                                           self.rnn1_size])
            self.rnn1_2_hidden_states = tf.reshape (self.rnn1_2_state,
                                                    shape=[-1, self.rnn1_num_layers * self.rnn1_count_hidden_types,
                                                           self.rnn1_size])

            # ------------ End : Encoder ----------------#

            # ------------ Start : Attention layer ----- #
            self.a = self.rnn1_1_hidden_states
            self.b = self.rnn1_1_output
            self.c = self.rnn1_2_hidden_states
            self.d = self.rnn1_2_output

            # get the last time step outputs
            last_ts_idx_st = (self.a).get_shape ().as_list ()[1] - 1
            last_ts_idx_op = (self.b).get_shape ().as_list ()[1] - 1

            self.a = self.a[:, last_ts_idx_st, :]
            self.b = self.b[:, last_ts_idx_op, :]
            self.c = self.c[:, last_ts_idx_st, :]
            self.d = self.d[:, last_ts_idx_op, :]

            # Collate these into forming the attention vector
            self.att_vecs = tf.stack ([self.a, self.b, self.c, self.d], axis=1)

            dim_gathered_states = self.att_vecs.get_shape ().as_list ()
            print 'dimension of attention vectors', dim_gathered_states

            self.num_att_vecs = dim_gathered_states[1]
            self.size_att_vec = dim_gathered_states[2]

            # For each attention vector, have a Weight and Bias to pass them through an activation function
            att_W_dims = [self.size_att_vec, 1]
            att_B_dims = [1]
            self.att_W = [tf.Variable (tf.random_normal (att_W_dims, stddev=0.1))
                          for _ in range (self.num_att_vecs)]
            self.att_B = [tf.Variable (tf.random_normal (att_B_dims, stddev=0.1, mean=0.58))
                          for _ in range (self.num_att_vecs)]

            # Scalar attention weights of each attention vector
            vec_wts = []
            for i in range (self.num_att_vecs):
                x_ = self.att_vecs[:, i, :]
                alpha = tf.sigmoid (tf.nn.xw_plus_b (x_, self.att_W[i], self.att_B[i]))
                vec_wts.append (alpha)

            self.vec_wts = tf.convert_to_tensor (vec_wts)
            self.vec_wts = tf.transpose (self.vec_wts, [1, 0, 2])
            sum_all = tf.reduce_sum (self.vec_wts, axis=2)
            sum_all = tf.reshape (sum_all, [-1, self.num_att_vecs, 1])
            self.vec_wts = self.vec_wts / sum_all

            z1 = tf.reshape (self.att_vecs, [-1, self.num_att_vecs, self.rnn1_size])
            z2 = tf.tile (self.vec_wts, [1, 1, self.rnn1_size])
            z2 = tf.stack ([z1, z2], axis=2)
            z2 = tf.reduce_prod (z2, axis=2)
            self.sum_wtd_att_wts = tf.reduce_mean (z2, axis=1)

            # -------- End: Attention layer ------- #

            # --------- Start : DECODER -----------#

            self.rnn2 = self.rnn_layer (num_layers=self.rnn2_num_layers,
                                        rnn_size=self.rnn2_size,
                                        batch_size=self.rnn2_batch_size,
                                        type='GRU'
                                        )

            self.rnn1_2_inp_features = self.size_att_vec
            rnn2_inp = tf.reshape (self.sum_wtd_att_wts, [-1, self.rnn2_timestep, self.rnn1_2_inp_features])

            print ' shape of rnn2 input', rnn2_inp.get_shape ().as_list ()

            self.rnn2_init_value = tf.placeholder (tf.float32,
                                                   shape=[None, self.rnn2_hidden_state_size],
                                                   name="rnn2_init_value")
            print ' rnn2_init_value ', self.rnn2_init_value.get_shape ().as_list ()

            with tf.variable_scope ('rnn2'):
                self.rnn2_output, self.rnn2_state = tf.nn.dynamic_rnn (self.rnn2,
                                                                       rnn2_inp,
                                                                       initial_state=self.rnn2_init_value,
                                                                       dtype=tf.float32)

            # --------- End  : DECODER ----------#

            rnn2_op_reshaped = self.rnn2_output[:, self.rnn2_timestep - 1, :]
            rnn2_op_reshaped = tf.reshape (rnn2_op_reshaped, [-1, self.rnn2_size])
            self.final_W = tf.Variable (tf.random_normal ([self.rnn2_size, 1], stddev=0.01))
            self.final_B = tf.Variable (tf.random_normal ([1], stddev=0.01))
            self.multip_res = tf.nn.xw_plus_b (x=rnn2_op_reshaped, weights=self.final_W, biases=self.final_B)
            self.output = tf.nn.relu (self.multip_res)
            # batch_time_shape = tf.shape (outputs)
            self.loss = tf.losses.mean_squared_error (
                labels=self.y,
                predictions=self.output
            )

            self.optimize = tf.train.AdamOptimizer (self.learning_rate, 0.9).minimize (self.loss)

            self.o = self.loss
            self.p = self.optimize

        print 'Initialzing netwwork done'
        print '------'

    ##############################################
    # Input:
    # X is a single element
    # init_zero_state
    # set true in 1st run, then on false
    ##############################################
    def run_step(self, x, init_zero_state = True):

        if init_zero_state:
            rnn1_1_init_value = np.zeros ([self.rnn1_hidden_state_size ])
            rnn1_2_init_value = np.zeros ([self.rnn1_hidden_state_size])
            rnn2_init_value = np.zeros ([self.rnn2_hidden_state_size ])
        else:
            rnn1_1_init_value = self.rnn1_1_last_state
            rnn1_2_init_value = self.rnn1_2_last_state
            rnn2_init_value = self.rnn2_last_state

        # Todo : Overhaul !!!
        out, rnn1_1_state, rnn1_1_state, rnn2_state = \
            self.session.run (
                [self.output, self.rnn1_1_state, self.rnn1_2_state, self.rnn2_state],
                feed_dict={
                    self.x: [x],
                    self.rnn1_1_init_value: [rnn1_1_init_value],
                    self.rnn1_2_init_value: [rnn1_2_init_value],
                    self.rnn2_init_value: [rnn2_init_value]
                }
            )


        self.rnn1_1_last_state = rnn1_1_state[0]
        self.rnn1_2_last_state = rnn1_1_state[0]
        self.rnn2_last_state = rnn2_state[-1,:]
        print self.rnn1_1_last_state.shape
        print self.rnn2_last_state.shape
        return out

    ###########################################################

    # x must be (batch_size, timesteps, input_size)
    # y must be (batch_size, timesteps, output_size)

    ###########################################################
    def train_batch(self, x, y):

        batch_size = x.shape[0]
        print 'batch size', batch_size

        rnn1_1_init_value = np.zeros ([batch_size, self.rnn1_hidden_state_size])
        rnn1_2_init_value = np.zeros ([batch_size, self.rnn1_hidden_state_size])
        rnn2_init_value = np.zeros ([batch_size, self.rnn2_hidden_state_size])
        loss, _ = self.session.run ([self.loss, self.optimize],
                                    feed_dict={
                                        self.x: x,
                                        self.y: y,
                                        self.rnn1_1_init_value: rnn1_1_init_value,
                                        self.rnn1_2_init_value: rnn1_2_init_value,
                                        self.rnn2_init_value: rnn2_init_value}
                                    )
        print loss
        return loss


######################################################################################

def get_data(batch, rows, lookback, num_channels):
    x = []
    y = []

    for _ in range (batch):
        a = np.random.randn (rows, lookback, num_channels)
        x.append (a)
        y.append (random.random ())

    x = np.asarray (x)
    x = np.reshape (x, [-1, rows, lookback, num_channels])
    y = np.asarray (y)
    y = np.reshape (y, [-1, 1])
    print 'data x shape', x.shape, 'data y shape', y.shape
    return x, y


###########################################################################################

num_channels = 4
rows = 3
lookback = 8
learning_rate = 0.003

session = tf.Session ()
network = Model (name="m1", session=session)
session.run (tf.initialize_all_variables ())
conv_inp_batch_size = 25
x, y = get_data (conv_inp_batch_size, rows, lookback, num_channels)
o = network.train_batch (x, y)

print'-----'
print o
print '-----'

pred_value = network.run_step(x[0], True)
