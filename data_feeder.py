import sys

sys.path.append ("./..")

import numpy as np
import config
import ili_datapipeline.DataFrame.generate_model_input_1 as inp_generator
import ili_datapipeline.common_utils as data_common_utils


class data_feeder_obj:
    def __init__(self):
        self.ilnei_data = None
        self.weather_data = None
        self.gst_data = None
        self.epi_week = None
        self.ili_y = None
        self.num_batches = 0
        self.get_one_epoch ()
        return

    def get_one_epoch(self):
        start_year = config.start_year
        end_year = config.end_year

        ili_lookback = config.ili_lookback
        time_step = config.time_step
        gst_lookback = config.gst_lookback
        ew_lookback = config.ew_lookback
        region = config.region
        var = False
        feature_stat_map = config.feature_stat_map
        ili_data, weather_data, gst_data, epi_week, ili_y = \
            inp_generator.gen_epoch_data (start_year,
                                          end_year,
                                          ili_lookback,
                                          gst_lookback,
                                          ew_lookback,
                                          region,
                                          var,
                                          feature_stat_map,
                                          time_step
                                          )

        self.num_batches = (np.shape (ili_data))[0]
        self.ili_data = np.split (ili_data, self.num_batches, axis=0)
        self.weather_data = np.split (weather_data, self.num_batches, axis=0)
        self.gst_data = np.split (gst_data, self.num_batches, axis=0)
        self.ili_y = np.split (ili_y, self.num_batches, axis=0)

    def _batch(self):
        for i in range (self.num_batches):
            yield (self.ili_data[i], self.weather_data[i], self.gst_data[i], self.ili_y[i])

    def get_batch(self):
        return self._batch ().next ()
