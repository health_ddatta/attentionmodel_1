import sys
sys.path.append("./..")

import config
import model_1_v2 as model
import tensorflow as tf
import data_feeder
import time
import matplotlib.pyplot as plt
import os

###########################################################################################

op_location = "./output"

def write_op(train_dict, learning_rate, features_stat_map, test_dict):

    cwd = os.getcwd()
    global op_location
    os.chdir(op_location)

    time_str =  str(time.time())
    directory = time_str
    if not os.path.exists(directory):
        os.makedirs(directory)
    os.chdir(directory)

    #### Record Training data

    file_name = time_str + '_train.txt'
    file_path = file_name
    f = open(file_path, 'w')

    eps = [str(ep) for ep in train_dict.keys()]
    eps = ','.join(eps)
    f.write(eps)
    f.write("\n")

    vals = [str(val) for val in train_dict.values()]
    vals = ','.join(vals)
    f.write(vals)
    f.write("\n")

    f.write(str(features_stat_map))
    f.write("\n")
    f.write("\n")
    f.write('Learning rate : ' + str(learning_rate))
    f.write("\n")
    f.close()

    img_name = time_str + '_train.png'
    plot_x = train_dict.keys()
    plot_y = train_dict.values()

    plt.grid()
    plt.plot(plot_x, plot_y, 'ro-')
    plt.xlabel('Epoch')
    plt.xlabel('Training Loss')
    plt.title('Training error (MSE) vs Epochs ')
    plt.savefig(img_name)
    plt.close()

    #### Record  test data
    img_name = time_str + 'test.png'
    test_x = []
    test_y1 = []
    test_y2 = []

    for week, item in test_dict.iteritems():
        test_x.append(week)
        test_y1.append(item[0])
        test_y2.append(item[1])

    plt.grid()
    plt.plot(test_x, test_y1, 'ro-')
    plt.plot(test_x, test_y2, 'bo-')
    plt.xlabel('Week')
    plt.xlabel('Predicted Value')
    plt.title('REAL vs PREDICTED wIlI ')
    plt.savefig(img_name)
    plt.close()

    # Get back to old directory
    os.chdir(cwd)
    return



###########################################################################################


def test_model(model_obj ,region , lookback, f_s_map ):
    test_x, test_y = data_feeder.get_test_data(region, lookback, feature_stat_map=f_s_map)

    # format :  week : [ predicted value, actual value ]
    test_data_dict = {}
    test_wk = 1

    for _x, _y in zip(test_x, test_y):
        res = model_obj.test_data(_x)[0][0]
        print ' > ', _y[0], ' -- ', res
        test_data_dict[test_wk] = [res, _y[0]]
        test_wk += 1

    return test_data_dict


###########################################################################################


def run_instance(region = 'nat', f_s_map = None):

    if f_s_map  is None :
        f_s_map = config.features_stat_map

    num_channels = 0
    for k,v in f_s_map.iteritems():
        num_channels += len(v)
    num_channels += 1

    rows = config.cnn_inp_height
    lookback = config.cnn_inp_width
    learning_rate = 0.1

    max_epochs = 5
    disp_interval = 1

    session = tf.Session()
    network = model.Model(session, learning_rate, rows, lookback, num_channels, name="model_1")
    session.run(tf.global_variables_initializer())

    loss_epoch_map = {}

    for epoch_num in range(max_epochs):

        epoch_x, epoch_y = data_feeder.get_one_epoch_data(region)
        for _x, _y in zip(epoch_x, epoch_y):
            loss = network.train_batch(_x, _y)

        if epoch_num % disp_interval == 0:
            print 'Loss', loss
            loss_epoch_map[epoch_num] = loss

    test_dict = test_model(network, region , lookback, f_s_map)
    write_op(loss_epoch_map, learning_rate, f_s_map , test_dict)
    session.close()


run_instance()


