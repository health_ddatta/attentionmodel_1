import tensorflow as tf
import numpy as np
import random
import data_feeder


class Model:
    def max_ppol(self, x_, window_shape, dilation_rate=1, stride=[1, 1]):
        op = tf.nn.pool(
            input=x_,
            window_shape=window_shape,
            pooling_type='MAX',
            padding='VALID',
            dilation_rate=dilation_rate,
            strides=stride,
            name=None,
            data_format=None
        )
        return op

    def avg_ppol(self, x_, window_shape, dilation_rate=1, stride=[1, 1]):
        op = tf.nn.pool(
            input=x_,
            window_shape=window_shape,
            pooling_type='AVG',
            padding='VALID',
            dilation_rate=dilation_rate,
            strides=stride,
            name=None,
            data_format=None
        )
        return op

    # conv layer
    # Input : x_
    def conv2d(self, x_, filter_dim, img_dim, stride=1, conv_name='conv'):
        data_format = 'channels_last'
        filter_ht = filter_dim[0]
        filter_wd = filter_dim[1]
        filter_count = filter_dim[2]

        activation = tf.layers.conv2d(
            inputs=x_,
            filters=filter_count,
            kernel_size=[filter_ht, filter_wd],
            strides=(stride, stride),
            padding='valid',
            data_format=data_format,
            dilation_rate=(1, 1),
            activation=None,
            use_bias=True,
            kernel_initializer=None,
            bias_initializer=tf.zeros_initializer(),
            kernel_regularizer=None,
            bias_regularizer=None,
            activity_regularizer=None,
            kernel_constraint=None,
            bias_constraint=None,
            trainable=True,
            name=conv_name,
            reuse=None
        )
        return activation

    def rnn_layer(self, num_layers, rnn_size, type='LSTM'):

        cells = []
        for i in range(num_layers):
            cell = None

            if type == 'LSTM':
                cell = tf.contrib.rnn.BasicLSTMCell(rnn_size,
                                                    forget_bias=1.0,
                                                    state_is_tuple=False)

            if type == 'GRU':
                cell = tf.contrib.rnn.GRUCell(rnn_size)

            cell = tf.nn.rnn_cell.DropoutWrapper(cell, self.dropout_prob)

            cells.append(cell)

        rnn_cell = tf.contrib.rnn.MultiRNNCell(cells, state_is_tuple=False)
        return rnn_cell

    def set_hyperparameters(self, name, session, learning_rate):
        # ---  hyperparameters --- #
        self.scope = name
        self.session = session
        self.learning_rate = learning_rate
        self.dropout_prob = 0.15

    def set_CNN_params(self, cnn_inp_height, cnn_inp_width, cnn_input_channels):
        # CNN details
        # 1 : cur_year
        # 2 : cur_year - 1
        # 3 : cur_year - 2
        # 4 : historical avg

        self.num_parallel_inputs = 4

        # number of exogenous features
        self.conv_num_channels = cnn_input_channels
        self.conv_inp_rows = cnn_inp_height
        self.conv_lookback = cnn_inp_width

        self.conv_inpx_dim = [self.conv_inp_rows, self.conv_lookback, self.conv_num_channels]
        self.conv1_num_filter = 64
        self.conv_filter_width = self.conv_inp_rows
        self.conv_filter_height = 2

    def set_encoder_params(self):

        # Encoder RNN details
        self.rnn1_count_hidden_types = 2

        # 1st RNN layer should have internally how many layers
        self.rnn1_num_layers = 3
        # 1st RNN layer should have internally how many states
        self.rnn1_size = 64
        self.rnn1_hidden_state_size = self.rnn1_num_layers * self.rnn1_count_hidden_types * self.rnn1_size

        # Decoder RNN details
        self.rnn2_count_hidden_types = 1
        self.rnn2_num_layers = 2
        self.rnn2_size = 64
        self.rnn2_hidden_state_size = self.rnn2_num_layers * self.rnn2_count_hidden_types * self.rnn2_size

        # Last state of rnn1 and rnn2, used when running the network in TEST mode
        self.rnn1_last_state = np.zeros([self.rnn1_hidden_state_size, ])
        self.rnn2_last_state = np.zeros([self.rnn2_hidden_state_size, ])

        self.rnn1_last_state_val = None
        self.rnn2_last_state_val = None

        return

    def __init__(self, session, learning_rate, cnn_inp_height, cnn_inp_width, cnn_input_channels, name="ModelInstance"):

        print 'Initialzing netwwork'
        self.set_hyperparameters(name, session, learning_rate)
        self.set_CNN_params(cnn_inp_height, cnn_inp_width, cnn_input_channels)
        self.set_encoder_params()

        # This is for testing phase
        self.init_zero_state = True

        with tf.variable_scope(self.scope):
            # x : [ batch_size, # parallel inputs, in_size ]
            # x is the input to conv layer
            self.x = tf.placeholder(tf.float32, [None,
                                                 self.num_parallel_inputs,
                                                 self.conv_inp_rows,
                                                 self.conv_lookback,
                                                 self.conv_num_channels],
                                    name="x")
            # y : [ batch_size, wILI Value ]
            # y is the next week's ILI value
            self.y = tf.placeholder(tf.float32, [None, 1])

            ########################

            filter_dim = [self.conv_filter_width, self.conv_filter_height, self.conv1_num_filter]
            conv_op_list = []
            window_shape = [1, 2]
            dilation_rate = [1, 1]

            for i in range(self.num_parallel_inputs):
                conv_i_inp = self.x[:, i, :, :, :]

                conv_i_op = self.conv2d(conv_i_inp,
                                        filter_dim,
                                        self.conv_inpx_dim,
                                        stride=1,
                                        conv_name='conv_' + str(i)
                                        )
                maxp_i_op = self.max_ppol(conv_i_op, window_shape, dilation_rate)
                avgp_i_op = self.avg_ppol(conv_i_op, window_shape, dilation_rate)

                pooled_op = tf.concat([maxp_i_op, avgp_i_op], axis=1)
                pooled_op_shape = pooled_op.get_shape().as_list()
                num_features = pooled_op_shape[1] * pooled_op_shape[2] * pooled_op_shape[3]
                pooled_op = tf.reshape(pooled_op, [-1, num_features])

                conv_op_list.append(pooled_op)

            self.conv_op_list = tf.stack(conv_op_list, axis=1)

            # --- End of Convolution -- #

            # ------------ Start : Encoder --------------#

            self.rnn1_list = []
            for i in range(self.num_parallel_inputs):
                rnn1_i = self.rnn_layer(
                    num_layers=self.rnn1_num_layers,
                    rnn_size=self.rnn1_size
                )
                self.rnn1_list.append(rnn1_i)

            self.rnn1_init_value = tf.placeholder(tf.float32,
                                                  shape=[None, self.num_parallel_inputs, self.rnn1_hidden_state_size],
                                                  name='rnn1_init_value')

            print ' >>> rnn 1 init value shape ', self.rnn1_init_value.get_shape().as_list()
            rnn1_output = []
            rnn1_state = []

            for i in range(self.num_parallel_inputs):
                rnn1_i = self.rnn1_list[i]
                rnn1_i_inp = self.conv_op_list[:, i, :]
                rnn1_i_inp_last_dim = rnn1_i_inp.get_shape().as_list()[-1]
                rnn1_i_inp = tf.reshape(rnn1_i_inp, [-1, 1, rnn1_i_inp_last_dim])

                rnn1_i_init_value = self.rnn1_init_value[:, i, :]
                scope_name = 'rnn1_' + str(i)

                with tf.variable_scope(scope_name):
                    output, state = tf.nn.dynamic_rnn(rnn1_i,
                                                      rnn1_i_inp,
                                                      initial_state=rnn1_i_init_value,
                                                      dtype=tf.float32)
                    rnn1_output.append(output)
                    rnn1_state.append(state)

            self.rnn1_output = tf.stack(rnn1_output, axis=1)
            print 'rnn1_output ', self.rnn1_output.get_shape()

            self.rnn1_state = tf.stack(rnn1_state, axis=1)
            print 'rnn1_state ', self.rnn1_state.get_shape()


            # Store this last state
            self.rnn1_last_state = self.rnn1_state
            print 'rnn1_last_state ', self.rnn1_last_state.get_shape()



            # Get the last time step output from each RNN
            self.rnn1_output = self.rnn1_output[:, :, -1, :]

            # Collate these into forming the attention vector
            self.att_vecs = self.rnn1_output

            dim_gathered_states = self.att_vecs.get_shape().as_list()
            # print 'dimension of attention vectors', dim_gathered_states

            self.num_att_vecs = dim_gathered_states[1]
            self.size_att_vec = dim_gathered_states[2]

            # print 'Number of attention vectors', self.num_att_vecs
            # print 'Size of attention vectors', self.size_att_vec

            att_W_dims = [self.size_att_vec, 1]
            att_B_dims = [1]

            self.att_W = [tf.Variable(tf.random_normal(att_W_dims, stddev=0.1, mean=0))
                          for _ in range(self.num_att_vecs)]
            self.att_B = [tf.Variable(tf.random_normal(att_B_dims, stddev=0.1, mean=0))
                          for _ in range(self.num_att_vecs)]

            # Scalar attention weights of each attention vector
            vec_wts = []
            for i in range(self.num_att_vecs):
                x_ = self.att_vecs[:, i, :]
                alpha = tf.sigmoid(tf.nn.xw_plus_b(x_, self.att_W[i], self.att_B[i]))
                vec_wts.append(alpha)

            self.vec_wts = tf.convert_to_tensor(vec_wts)
            # print ' shape of self.vec_wts', self.vec_wts.get_shape().as_list()

            self.vec_wts = tf.transpose(self.vec_wts, [1, 0, 2])
            # print ' shape of self.vec_wts', self.vec_wts.get_shape().as_list()

            self.vec_wts = tf.convert_to_tensor(vec_wts)
            self.vec_wts = tf.transpose(self.vec_wts, [1, 0, 2])
            sum_all = tf.reduce_sum(self.vec_wts, axis=2)
            sum_all = tf.reshape(sum_all, [-1, self.num_att_vecs, 1])
            self.vec_wts = self.vec_wts / sum_all

            z1 = tf.reshape(self.att_vecs, [-1, self.num_att_vecs, self.rnn1_size])
            z2 = tf.tile(self.vec_wts, [1, 1, self.rnn1_size])
            z2 = tf.stack([z1, z2], axis=2)
            z2 = tf.reduce_prod(z2, axis=2)
            self.sum_wtd_att_wts = tf.reduce_mean(z2, axis=1)

            # -------- End: Attention layer ------- #

            # --------- Start : DECODER -----------#

            self.rnn2 = self.rnn_layer(num_layers=self.rnn2_num_layers,
                                       rnn_size=self.rnn2_size,
                                       type='GRU'
                                       )

            self.rnn1_2_inp_features = self.size_att_vec
            self.rnn2_timestep = 1
            rnn2_inp = tf.reshape(self.sum_wtd_att_wts, [-1, self.rnn2_timestep, self.rnn1_2_inp_features])

            print ' shape of rnn2 input', rnn2_inp.get_shape().as_list()

            self.rnn2_init_value = tf.placeholder(tf.float32,
                                                  shape=[None, self.rnn2_hidden_state_size],
                                                  name="rnn2_init_value")

            print ' rnn2_init_value ', self.rnn2_init_value.get_shape().as_list()

            with tf.variable_scope('rnn2'):
                self.rnn2_output, self.rnn2_state = tf.nn.dynamic_rnn(self.rnn2,
                                                                      rnn2_inp,
                                                                      initial_state=self.rnn2_init_value,
                                                                      dtype=tf.float32)

            # --------- End  : DECODER ----------#
            rnn2_op_last_ts = self.rnn2_output[:, self.rnn2_timestep - 1, :]

            rnn2_op_reshaped = tf.reshape(rnn2_op_last_ts, [-1, self.rnn2_size])
            self.final_W = tf.Variable(tf.random_normal([self.rnn2_size, 1], stddev=0.01))
            self.final_B = tf.Variable(tf.random_normal([1], stddev=0.01))
            self.multip_res = tf.nn.xw_plus_b(x=rnn2_op_reshaped, weights=self.final_W, biases=self.final_B)
            self.output = tf.nn.relu(self.multip_res)

            # Saving this for use in testing phase
            self.rnn2_last_state = self.rnn2_state

            self.loss = tf.losses.mean_squared_error(
                labels=self.y,
                predictions=self.output
            )

            # self.optimize = tf.train.AdamOptimizer (self.learning_rate).minimize (self.loss)

            optimizer = tf.train.AdamOptimizer(self.learning_rate)
            gradients, variables = zip(*optimizer.compute_gradients(self.loss))
            gradients, _ = tf.clip_by_global_norm(gradients, 5.0)
            self.optimize = optimizer.apply_gradients(zip(gradients, variables))

        print 'Initialzing network done'
        print '------'

    def train_batch(self, x, y):
        rnn1_init_value = np.zeros([x.shape[0], self.num_parallel_inputs, self.rnn1_hidden_state_size])
        rnn2_init_value = np.zeros([x.shape[0], self.rnn2_hidden_state_size])

        loss, _rnn1_last_value, _rnn2_last_value, _ = self.session.run(
            [self.loss,
             self.rnn1_last_state,
             self.rnn2_last_state,
             self.optimize],
            feed_dict={
                self.x: x,
                self.y: y,
                self.rnn1_init_value: rnn1_init_value,
                self.rnn2_init_value: rnn2_init_value
            })

        self.rnn1_last_state_val = _rnn1_last_value
        self.rnn2_last_state_val = _rnn2_last_value
        return loss

    # present a single test instance frame
    def test_data(self, x):

        if self.init_zero_state :
            self.init_zero_state = False
            rnn1_init_value = np.zeros([1, self.num_parallel_inputs, self.rnn1_hidden_state_size])
            rnn2_init_value = np.zeros([1, self.rnn2_hidden_state_size])

        else:

            rnn1_init_value = self.rnn1_last_state_val
            rnn2_init_value = self.rnn2_last_state_val


        output, _rnn1_last_value, _rnn2_last_value = self.session.run([self.output,
                                                                       self.rnn1_last_state,
                                                                       self.rnn2_last_state
                                                                       ],
                                                                      feed_dict={
                                                                          self.x: [x],
                                                                          self.rnn1_init_value: rnn1_init_value,
                                                                          self.rnn2_init_value: rnn2_init_value
                                                                      })

        self.rnn1_last_state_val = _rnn1_last_value
        self.rnn2_last_state_val = _rnn2_last_value
        return output


######################################################################################

def get_data(batch, rows, lookback, num_channels):
    x = []
    y = []

    for _ in range(batch):
        a = np.random.randn(4, rows, lookback, num_channels)
        a = np.transpose(np.reshape(a, [-1, num_channels, rows, lookback]), [0, 2, 3, 1])
        print a.shape
        x.append(a)
        y.append(random.random())

    x = np.asarray(x)
    x = np.reshape(x, [-1, 4, rows, lookback, num_channels])
    print x.shape

    y = np.asarray(y)
    y = np.reshape(y, [-1, 1])
    print 'data x shape', x.shape, 'data y shape', y.shape
    return x, y

# x,y = get_data(batch=2, rows=3, lookback=8, num_channels=4)
