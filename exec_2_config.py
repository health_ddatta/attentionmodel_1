conf = [
    {
        'abs_hum': ['week_median', 'week_max_daily_variation'],
        'rel_hum': ['week_median'],
        'sp_hum': [],
        'temp': ['week_max','week_min','week_variance'],
        'u_wind': [],
        'v_wind': []
    }

    ,
    {
        'abs_hum': ['week_median', 'week_max_daily_variation', 'week_variance'],
        'rel_hum': ['week_median'],
        'sp_hum': [],
        'temp': ['week_median'],
        'u_wind': [],
        'v_wind': []
    }
    ,

    {
        'abs_hum': ['week_median', 'week_max_daily_variation', 'week_variance'],
        'rel_hum': ['week_median'],
        'sp_hum': ['week_median'],
        'temp': ['week_median'],
        'u_wind': ['week_median'],
        'v_wind': ['week_median']
    }
    ,

    {
        'abs_hum': ['week_median', 'week_variance'],
        'rel_hum': ['week_median'],
        'sp_hum': ['week_variance'],
        'temp': ['week_median', 'week_variance'],
        'u_wind': ['week_variance'],
        'v_wind': ['week_variance']
    }
    ,

    {
        'abs_hum': ['week_median', 'week_max_daily_variation', 'week_variance'],
        'rel_hum': ['week_median'],
        'sp_hum': ['week_max_daily_variation'],
        'temp': ['week_median'],
        'u_wind': ['week_min'],
        'v_wind': ['week_max']
    }
    ,

    {
        'abs_hum': ['week_median', 'week_min_daily_mean', 'week_variance'],
        'rel_hum': ['week_median'],
        'sp_hum': ['week_median'],
        'temp': ['week_median', 'week_max_daily_variation'],
        'u_wind': ['week_min_daily_mean'],
        'v_wind': ['week_min_daily_mean']
    }
    ,

    {
        'abs_hum': ['week_median', 'week_max_daily_variation', 'week_variance'],
        'rel_hum': ['week_median'],
        'sp_hum': ['week_min_daily_mean'],
        'temp': ['week_median', 'week_max_daily_variation'],
        'u_wind': ['week_min'],
        'v_wind': ['week_min']
    }
    ,

]


conf_old =[ {'sp_hum': [], 'abs_hum': ['week_median', 'week_max_daily_variation'], 'temp': ['week_median'], 'rel_hum': [], 'u_wind': [], 'v_wind': []}
]



stats = [
    'week_median',
    'week_max',
    'week_min',
    'week_variance',
    'week_max_daily_variation',
    'week_min_daily_mean']
